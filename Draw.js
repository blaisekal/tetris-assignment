'use strict';

function Draw() {
    this.drawList = [];
    this.canvas = document.querySelector('canvas');
    this.context = this.canvas.getContext('2d');
    this.paintLoop();
}

Draw.prototype = {

    register: function(thing) {
        this.drawList.push(thing);
    },

    unregister: function(thing) {
        var index = this.drawList.indexOf(thing);
        this.drawList.splice(index, 1);
    },

    paintLoop: function() {
        requestAnimationFrame(this.paintLoop.bind(this));
        this.drawThings();
    },

    drawThings: function() {
        this.context.fillStyle = '#fff';
        this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
        for (var i = 0, m = this.drawList.length; i < m; i++) {
            this.drawList[i].draw(this.context, CONST.TILE_SIZE, CONST.WIDTH, CONST.HEIGHT);
        }
    }

};
